from django.apps import AppConfig


class AppHavefunConfig(AppConfig):
    name = 'app_havefun'
